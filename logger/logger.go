package logger

import (
	"fmt"
	"path/filepath"
	"runtime"

	"bitbucket.org/indigofeather/result-center/configs"
	"git.polaris/polaris/log"
	"github.com/pkg/errors"
)

var Log *log.Bee

func New() *log.Bee {
	if Log == nil {
		conf := configs.Instance("log")
		setting := log.NewConfig()
		setting.ChannelLen = conf.GetInt64("channelLen")
		setting.SlackURL = conf.GetString("slackUrl")
		setting.FileName = filepath.Join(getLogDir(), conf.GetString("filename"))
		setting.IsToConsole = conf.GetBool("toConsole")
		setting.IsToFile = conf.GetBool("toFile")
		Log = log.New(setting)
	}
	return Log
}

func getLogDir() string {
	_, filename, _, _ := runtime.Caller(0)
	path := filepath.Join(filepath.Dir(filename), "../logs/")
	base := filepath.Join(filepath.Dir(filename), "../")
	relPath, _ := filepath.Rel(base, path)
	return relPath
}

// ThrowError 依設定檔格式化印出log及slack通知
func ThrowError(customErr, originErr error) {
	l := New()
	l.Error(fmt.Sprintf("%+v\n\n+----------------------------------------------------+\n", errors.Wrapf(customErr, "\nOrigin Err: %v\n", originErr)))
}
