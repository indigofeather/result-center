package ws

import (
	"encoding/json"
	"fmt"
	"time"

	"bitbucket.org/indigofeather/result-center/round"
	melody "gopkg.in/olahol/melody.v1"
)

var WS *melody.Melody

// RequestForm ws 接口格式
type RequestForm struct {
	Action    string `json:"action"`
	StartTime int64  `json:"start_time"`
	EndTime   int64  `json:"end_time"`
	RoundID   int    `json:"round_id"`
}

func init() {
	WS = melody.New()
	WS.Config.MaxMessageSize = 10000000

	WS.HandleMessage(func(s *melody.Session, msg []byte) {
		req := &RequestForm{}
		err := json.Unmarshal(msg, req)
		if err != nil {
			fmt.Println("request unmarshal err", err)
			return
		}

		// 確認時間內期數
		if req.Action == "check" {
			list, err := round.GetRoundsByPeriod(time.Unix(req.StartTime, 0), time.Unix(req.EndTime, 0))
			if err != nil {
				fmt.Println("err", err)
				return
			}

			res := struct {
				Action    string             `json:"action"`
				RoundList []*round.ListRound `json:"round_list"`
			}{
				Action:    req.Action,
				RoundList: list,
			}
			bt, err := json.Marshal(res)
			if err != nil {
				return
			}

			err = Send(bt)
			fmt.Println(err)
			// 傳送有問題，寫log

			return
		}

		// 確認指定期數
		if req.Action == "round" {
			r, err := round.GetRoundByID(req.RoundID)
			if err != nil {
				fmt.Println("err", err)
				return
			}

			res := struct {
				Action string             `json:"action"`
				Data   *round.RoundDetail `json:"data"`
			}{
				Action: req.Action,
				Data:   r,
			}
			bt, err := json.Marshal(res)
			if err != nil {
				return
			}

			err = Send(bt)
			fmt.Println(err)
			// 傳送有問題，寫log

			return
		}

		WS.Broadcast(msg)
	})
}

// Send 發送websocket
func Send(msg []byte) error {
	return WS.Broadcast(msg)
}
