
-- +migrate Up

CREATE TABLE `history` (
  `id` int(11) UNSIGNED NOT NULL,
  `source_name` varchar(30) NOT NULL,
  `round_id` int(11) UNSIGNED NOT NULL,
  `result` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `rounds` (
  `id` int(11) UNSIGNED NOT NULL,
  `game_id` varchar(10) NOT NULL,
  `game_round` varchar(30) NOT NULL,
  `result` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `draw_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `checked` tinyint(4) NOT NULL DEFAULT 0,
  CONSTRAINT `Uni_Round` UNIQUE (`game_id`,`game_round`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `rounds`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `history`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `rounds`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

-- +migrate Down

DROP TABLE `history`;
DROP TABLE `rounds`;
