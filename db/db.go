package db

import (
	"database/sql"
	"log"
	"strconv"
	"time"

	"bitbucket.org/indigofeather/result-center/configs"
	// driver
	_ "github.com/go-sql-driver/mysql"
)

var DB *sql.DB

func init() {
	var conf = configs.Instance("db")
	user := conf.GetString("username")
	pwd := conf.GetString("password")
	host := conf.GetString("host")
	port := strconv.Itoa(conf.GetInt("port"))
	db := conf.GetString("db")
	readTimeout := conf.GetString("readTimeout")
	location := conf.GetString("location")
	connMaxLifetime := conf.GetInt("connMaxLifetime")
	maxOpenConns := conf.GetInt("maxOpenConns")
	maxIdleConns := conf.GetInt("maxIdleConns")

	var err error
	DB, err = sql.Open("mysql", user+":"+pwd+"@("+host+":"+port+")/"+db+"?parseTime=true&multiStatements=true&readTimeout="+readTimeout+"&loc="+location)
	DB.SetConnMaxLifetime(time.Duration(connMaxLifetime) * time.Minute)
	DB.SetMaxOpenConns(maxOpenConns)
	DB.SetMaxIdleConns(maxIdleConns)

	if err != nil {
		log.Fatal(err)
	}
}
