package consts

// 內部使用的賽果結構
type Result struct {
	Result map[string]int
	Stamp  int64
}
