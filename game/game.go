package game

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/indigofeather/result-center/configs"
	"github.com/spf13/viper"
)

// Game instance
type Game struct {
	ID             string
	Name           string
	Period         int
	RoundPerDay    int
	roundFirstTime string
}

// RoundDetail 預開期數格式
type RoundDetail struct {
	RoundID   string    `json:"roundid"`
	StartTime time.Time `json:"start_time"`
	CloseTime time.Time `json:"close_time"`
}

// Conf 取設定檔實體
func conf() *viper.Viper {
	return configs.Instance("games")
}

// Instance 回傳 game 模組
func Instance(g string) *Game {
	return &Game{
		ID:             conf().GetString("game." + g + ".id"),
		Name:           g,
		Period:         conf().GetInt("game." + g + ".period"),
		RoundPerDay:    conf().GetInt("game." + g + ".roundPerDay"),
		roundFirstTime: conf().GetString("game." + g + ".roundFirstTime"),
	}
}

// All 回傳所有遊戲列表
func All() []string {
	return conf().GetStringSlice("games")
}

// AllInstance 取所有遊戲實體
func AllInstance() []*Game {
	list := All()
	r := []*Game{}

	for _, v := range list {
		g := Instance(v)
		r = append(r, g)
	}

	return r
}

// GetPreOpenRounds 計算並返回預開期數資料 (以當日為計算起始, lastRound需傳入前一天最後一期)
// TODO 目前還沒有避開某天的邏輯
func (g *Game) GetPreOpenRounds(lastRound string, unit int) ([]*RoundDetail, error) {
	amount := unit * g.RoundPerDay
	rounds := []*RoundDetail{}

	// 算當天第一期開期時間
	now := time.Now()
	unix, err := g.generateFirstTimeOfGivenDay(now)
	if err != nil {
		return rounds, err
	}

	switch conf().GetString("game." + g.Name + ".roundCountMethod") {
	// 序號型期數
	case "serial":
		gameRound, err := strconv.Atoi(lastRound)
		if err != nil {
			return rounds, err
		}

		firstRound := gameRound + 1
		baseUnix := unix

		for i := 0; i < amount; i++ {
			// 每輪重置
			baseInt := i

			if mul := i / g.RoundPerDay; mul >= 0 {
				baseInt = i % g.RoundPerDay
			}

			// baseUnix更新 for 下一輪期數
			if i%g.RoundPerDay == 0 && i != 0 {
				baseUnix, err = g.generateFirstTimeOfGivenDay(time.Unix(unix, 0).AddDate(0, 0, i/g.RoundPerDay))
				if err != nil {
					return rounds, err
				}
			}

			r := g.generateRoundTime(baseUnix, baseInt)
			r.RoundID = strconv.Itoa(firstRound + i)
			rounds = append(rounds, r)
		}

		break
	// 日期加當日序號型期數
	case "order":
		index := [2]string{
			lastRound[:8],
			lastRound[8:],
		}
		digitLen := len(index[1])
		baseUnix := unix

		for i := 0; i < amount; i++ {
			// 每輪重置
			baseInt := i

			if mul := i / g.RoundPerDay; mul >= 0 {
				baseInt = i % g.RoundPerDay
			}

			// baseUnix更新 for 下一輪期數
			if i%g.RoundPerDay == 0 && i != 0 {
				baseUnix, err = g.generateFirstTimeOfGivenDay(time.Unix(unix, 0).AddDate(0, 0, i/g.RoundPerDay))
				if err != nil {
					return rounds, err
				}
			}
			r := g.generateRoundTime(baseUnix, baseInt)

			// 計算前段期號
			year, month, day := time.Unix(baseUnix, 0).Date()
			// round 號碼補零
			rid := fmt.Sprintf("%0*d", digitLen, baseInt+1)
			dateStr := strconv.Itoa(year) + fmt.Sprintf("%02d", int(month)) + fmt.Sprintf("%02d", day) + rid
			r.RoundID = dateStr
			rounds = append(rounds, r)
		}
	}

	return rounds, nil
}

// generateRoundTime 計算時間並組出預開資料對象
// @baseUnix 當天第一期開獎時間
// @offset   偏移量 (往後推幾期)
func (g *Game) generateRoundTime(baseUnix int64, offset int) *RoundDetail {
	period := g.Period * 60 * offset
	start := baseUnix + int64(period)
	close := start + int64(g.Period)*60
	if offset == 0 {
		start = baseUnix
		period = g.Period * 60
	}

	return &RoundDetail{
		StartTime: time.Unix(start, 0),
		CloseTime: time.Unix(close, 0),
	}
}

// generateFirstTimeOfGivenDay 組出給定日期當天的第一次開期時間
func (g *Game) generateFirstTimeOfGivenDay(t time.Time) (int64, error) {
	firstTime := strings.Split(g.roundFirstTime, ":")
	hour, err := strconv.Atoi(firstTime[0])
	if err != nil {
		return 0, err
	}
	min, err := strconv.Atoi(firstTime[1])
	if err != nil {
		return 0, err
	}

	// 北京時間時區 (UTC標準)
	zone, err := time.LoadLocation("Asia/Chongqing")
	if err != nil {
		return 0, err
	}

	baseTime := time.Date(t.Year(), t.Month(), t.Day(), hour, min, 0, 0, zone)

	return baseTime.Unix(), nil
}
