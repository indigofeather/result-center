package task

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"time"

	"bitbucket.org/indigofeather/result-center/consts"
	"bitbucket.org/indigofeather/result-center/db"
	"bitbucket.org/indigofeather/result-center/game"
	"bitbucket.org/indigofeather/result-center/logger"
	"bitbucket.org/indigofeather/result-center/models"
	"bitbucket.org/indigofeather/result-center/round"
	"bitbucket.org/indigofeather/result-center/source"
	"bitbucket.org/indigofeather/result-center/ws"
	"github.com/robfig/cron"
	"github.com/volatiletech/sqlboiler/queries/qm"
)

var ErrOnCheckLast = errors.New("[task], 確認剩餘期數時發生錯誤")

// 確認可信度
func checkCorrectness(results ...*consts.Result) (*consts.Result, bool) {
	// 邏輯:至少2個獎源
	// if len(results) < 2 {
	//  fmt.Println("獎源比對組數不足", results)
	// 	return nil, false
	// }

	sameCount := 0
	first, other := results[0], results[1:]
	if len(results) == 1 {
		return results[0], true
	}

	for _, v := range other {
		if reflect.DeepEqual(first, v) {
			sameCount++
		}
	}
	// TODO 比對沒過半數，應該換下一組再次比對
	if sameCount < len(results)/2 {
		return nil, false
	}

	return first, true
}

func checkLastAndPreOpen() {
	// 歷遍遊戲
	games := game.AllInstance()
	sources := source.AllInstance()

	for _, g := range games {
		// 以現在時間去確認剩餘場數（目前基準點是不足半輪時才預產下期）
		count, err := models.Rounds(db.DB, qm.Where("game_id = ? AND end_at > ?", g.Name, time.Now().Add(time.Hour*15*-1))).Count()
		if err != nil {
			logger.ThrowError(ErrOnCheckLast, err)
			continue
		}

		if int(count) > g.RoundPerDay/2 {
			continue
		}

		// 進行預產
		for _, src := range sources {
			roundid := src.GetLastRoundOfGivenDate(g, time.Now().AddDate(0, 0, -1))
			preRounds, err := g.GetPreOpenRounds(roundid, 2)
			if err != nil {
				continue
			}
			err = round.Forge(g, preRounds)
			if err != nil {
				fmt.Println("err on forge", err)
			}
		}
	}
}

func fetchResult() {
	// 遊戲清單
	games := game.AllInstance()
	// 獎源清單
	sources := source.AllInstance()

	for _, g := range games {
		// 歷遍奬源
		// 取到第一個有這款遊戲的獎源
		// 以取到的roundid到DB確認還沒有獎源的有哪些UndrawCheck，返回roundid清單
		// 然後其他獎源直接用GetRoundResult方式取，接在一個slice
		// 接著再全數丟入checkCorrectness比對

		// 第一次取出還沒有賽果的清單
		for _, s := range sources {
			// 跳過不含這款遊戲的獎源
			ok := s.HasGame(g)
			if !ok {
				continue
			}

			// 先取出所有獎源，確認之中有哪些還沒有賽果
			results := s.GetRecentResults(g)
			if len(results) < 1 {
				continue
			}
			undraw, err := round.GetUndrawRound(g, results)
			if err != nil {
				fmt.Println(err)
				// TODO 這邊應該是DB問題，如何處理
				continue
			}

			for _, i := range undraw {
				var testResults = []*consts.Result{}

				for _, sr := range sources {
					// 跳過不含這款遊戲的獎源
					ok := sr.HasGame(g)
					if !ok {
						continue
					}

					r, err := sr.GetRoundResult(g, i)
					if err != nil {
						continue
					}
					testResults = append(testResults, r)
				}

				if len(testResults) < 1 {
					continue
				}
				result, ok := checkCorrectness(testResults...)
				if !ok {
					continue
				}
				// 寫入
				roundFinal, err := round.SetResult(g, i, result)
				if err != nil {
					// 寫log
					// 可能是DB相關問題，需做什麼邏輯處理？
					continue
				}

				// 送ws給server
				type innerData struct {
					ID      uint           `json:"id"`
					Game    string         `json:"game_id"`
					RoundID string         `json:"round_id"`
					Result  map[string]int `json:"result"`
					Status  int8           `json:"status"`
				}
				message := struct {
					Action string     `json:"action"`
					Data   *innerData `json:"data"`
				}{
					Action: "result",
					Data: &innerData{
						ID:      roundFinal.ID,
						Game:    roundFinal.GameID,
						RoundID: roundFinal.GameRound,
						Result:  result.Result,
						Status:  roundFinal.Status,
					},
				}
				msgByte, err := json.Marshal(message)
				if err != nil {
					fmt.Println("marshal err", err)
					continue
				}
				ws.Send(msgByte)
			}
		}
	}
}

// Start 設定定時任務
func Start() {
	c := cron.New()
	// 確認期數
	c.AddFunc("@every 3m", checkLastAndPreOpen)
	// 取賽果
	c.AddFunc("@every 30s", fetchResult)
	// 手動執行第一次
	checkLastAndPreOpen()
	fetchResult()

	// 從基準點才開始定時
	ticker := time.NewTicker(1 * time.Second)
	quit := make(chan struct{})
	go func() {
		for {
			select {
			case <-ticker.C:
				if time.Now().Second()%10 == 0 {
					close(quit)
				}
			case <-quit:
				ticker.Stop()
				c.Start()
				checkLastAndPreOpen()
				fetchResult()
				return
			}
		}
	}()
}
