package configs

import (
	"fmt"
	"log"

	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

var (
	configs = map[string]*viper.Viper{}
	// Path for test
	tPath    = "./configs"
	path     = "../configs"
	fileType = "toml"
)

// Instance 設定檔實例化
func Instance(name string) *viper.Viper {
	if _, ok := configs[name]; !ok {
		configs[name] = load(name)
	}

	return configs[name]
}

func load(name string) *viper.Viper {
	v := viper.New()

	v.AddConfigPath(tPath)
	v.AddConfigPath(path)
	v.SetConfigType(fileType)
	v.SetConfigName(name)

	err := v.ReadInConfig()
	if err != nil {
		log.Fatal(err)
	}

	// live watch config files
	v.WatchConfig()
	v.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed:", e.Name)
		// 清除已記錄實體
		for k := range configs {
			delete(configs, k)
		}
	})

	return v
}
