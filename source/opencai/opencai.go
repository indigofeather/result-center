package opencai

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"

	"bitbucket.org/indigofeather/result-center/cache"
	"bitbucket.org/indigofeather/result-center/configs"
	"bitbucket.org/indigofeather/result-center/consts"
	"bitbucket.org/indigofeather/result-center/game"
)

var instance *Opencai
var once sync.Once
var ErrNoResult = errors.New("[opencai], 查無賽果資料")

// Opencai 開彩網 http://www.opencai.net/apifree/
type Opencai struct {
	name      string
	isRunning bool
}

func Instance() *Opencai {
	once.Do(func() {
		instance = &Opencai{name: "Opencai"}
	})

	return instance
}

// HasGame 回傳該獎源是否含蓋某款遊戲
func (o *Opencai) HasGame(g *game.Game) bool {
	conf := configs.Instance("source")
	list := conf.GetStringSlice(o.name + ".games")
	for _, src := range list {
		if src == g.Name {
			return true
		}
	}
	return false
}

func (o *Opencai) Name() string {
	return o.name
}

func (o *Opencai) Start() {
	o.isRunning = true
	println("Opencai start!")
}

func (o *Opencai) Stop() {
	o.isRunning = false
	println("Opencai stop!")
}

func (o *Opencai) Run() {
	if !o.isRunning {
		println("Opencai not running!")
	}

	// 1.解析賽果，塞到 history，前面已經做完的跳過
	// 2.確認抓的賽果 round.CheckResult()
}

// GetRecentResults 取近期賽果
func (o *Opencai) GetRecentResults(g *game.Game) map[string]*consts.Result {
	conf := configs.Instance("source")
	url := conf.GetString(o.Name() + ".url")
	url = strings.Replace(url, "%s", conf.GetString(o.name+"."+g.Name+".code"), -1)
	cacheKey := []byte("result_" + o.name + "_" + g.Name)

	dataList := struct {
		List []struct {
			Num       string `json:"expect"`
			Result    string `json:"opencode"`
			TimeStamp int64  `json:"opentimestamp"`
		} `json:"data"`
	}{}

	// get cache
	dataByte, _ := cache.Cache.Get(cacheKey)

	// 避免api頻繁撈取，間距3秒共重試 1 次，第一次就成功即跳出
	if len(dataByte) == 0 {
		for i := 0; i < 2; i++ {
			request, err := http.Get(url)
			if err != nil {
				continue
			}
			defer request.Body.Close()
			dataByte, err = ioutil.ReadAll(request.Body)
			if err == nil {
				cache.Cache.Set(cacheKey, dataByte, 5)
				// 成功跳出
				break
			}

			// 進入重試
			time.Sleep(3 * time.Second)
		}
	}

	err := json.Unmarshal(dataByte, &dataList)
	if err != nil {
		// 寫log
		fmt.Println("unmarshal err", string(dataByte))
		return nil
	}
	if len(dataList.List) == 0 {
		// 仍未成功取得，log記錄賽果異常
		// 通知賽果異常
		return nil
	}

	list := map[string]*consts.Result{}
	for _, v := range dataList.List {
		// 處理賽果
		frag := strings.Split(v.Result, ",")
		result := map[string]int{}
		for index, item := range frag {
			i, err := strconv.Atoi(item)
			if err != nil {
				return nil
			}
			result[strconv.Itoa(index+1)] = i
		}

		list[v.Num] = &consts.Result{
			Result: result,
			Stamp:  v.TimeStamp,
		}
	}

	return list
}

// GetRoundResult 取某round
func (o *Opencai) GetRoundResult(g *game.Game, rid string) (*consts.Result, error) {
	results := o.GetRecentResults(g)
	if value, ok := results[rid]; ok {
		return value, nil
	}
	return nil, ErrNoResult
}

// GetLastRoundOfGivenDate 取某日最後一筆期數
func (o *Opencai) GetLastRoundOfGivenDate(g *game.Game, day time.Time) string {
	conf := configs.Instance("source")
	url := conf.GetString(o.Name() + ".urlByDay")
	url = strings.Replace(url, "%s", conf.GetString(o.name+"."+g.Name+".code"), -1)
	url = strings.Replace(url, "%d", day.Format("2006-01-02"), -1)

	request, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
	}
	defer request.Body.Close()
	dataByte, err := ioutil.ReadAll(request.Body)
	if err != nil {
		log.Fatalln(err)
	}

	dataList := struct {
		List []struct {
			Num string `json:"expect"`
		} `json:"data"`
	}{}
	if err := json.Unmarshal(dataByte, &dataList); err != nil {
		log.Fatal(err)
	}

	if len(dataList.List) == 0 {
		return ""
	}

	return dataList.List[0].Num
}
