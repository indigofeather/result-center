package cpk

import (
	"errors"
	"sync"
	"time"

	"bitbucket.org/indigofeather/result-center/configs"
	"bitbucket.org/indigofeather/result-center/consts"
	"bitbucket.org/indigofeather/result-center/game"
)

// CPK 彩票控 https://www.caipiaokong.com/open/3.html
type CPK struct {
	name      string
	isRunning bool
}

var instance *CPK
var once sync.Once
var ErrNoResult = errors.New("[cpk], 查無賽果資料")

func Instance() *CPK {
	once.Do(func() {
		instance = &CPK{name: "CPK"}
	})

	return instance
}

// HasGame 回傳該獎源是否含蓋某款遊戲
func (o *CPK) HasGame(g *game.Game) bool {
	conf := configs.Instance("source")
	list := conf.GetStringSlice(o.name + ".games")
	for _, src := range list {
		if src == g.Name {
			return true
		}
	}
	return false
}

func (c *CPK) Name() string {
	return c.name
}

func (c *CPK) Start() {
	c.isRunning = true
	println("CPK start!")
}

func (c *CPK) Stop() {
	c.isRunning = false
	println("CPK stop!")
}

func (c *CPK) Run() {
	if !c.isRunning {
		println("CPK not running!")
	}

	// 1.解析賽果，塞到 history，前面已經做完的跳過
	// 2.確認抓的賽果 round.CheckResult()
}

func (o *CPK) GetRecentResults(g *game.Game) map[string]*consts.Result {
	return map[string]*consts.Result{}
}

// GetLastRound 取某日最後一筆期數
func (c *CPK) GetLastRoundOfGivenDate(g *game.Game, day time.Time) string {
	return ""
}

// GetRoundResult 取某round
func (o *CPK) GetRoundResult(g *game.Game, rid string) (*consts.Result, error) {
	results := o.GetRecentResults(g)
	if value, ok := results[rid]; ok {
		return value, nil
	}
	return nil, ErrNoResult
}
