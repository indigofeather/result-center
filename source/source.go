package source

import (
	"time"

	"bitbucket.org/indigofeather/result-center/configs"
	"bitbucket.org/indigofeather/result-center/consts"
	"bitbucket.org/indigofeather/result-center/game"
	"bitbucket.org/indigofeather/result-center/source/cpk"
	"bitbucket.org/indigofeather/result-center/source/opencai"
)

type Source interface {
	Name() string
	Start()
	Stop()
	Run()
	HasGame(g *game.Game) bool
	GetLastRoundOfGivenDate(*game.Game, time.Time) string
	GetRecentResults(*game.Game) map[string]*consts.Result
	GetRoundResult(*game.Game, string) (*consts.Result, error)
}

var (
	_ Source = &cpk.CPK{}
	_ Source = &opencai.Opencai{}
)

func Instance(name string) Source {
	var i Source
	switch name {
	case "CPK":
		i = cpk.Instance()
	case "Opencai":
		i = opencai.Instance()
	default:
		panic("Error: wrong source name")
	}

	return i
}

// All 取得所有獎源清單
func All() []string {
	conf := configs.Instance("source")
	return conf.GetStringSlice("sources")
}

// AllInstance 直接取得所有獎源實體清單
func AllInstance() []Source {
	all := All()
	inst := []Source{}
	for _, v := range all {
		inst = append(inst, Instance(v))
	}
	return inst
}

func Start(names []string) {
	for _, v := range names {
		go Instance(v).Start()
	}
}

func Stop(names []string) {
	for _, v := range names {
		go Instance(v).Stop()
	}
}

func Run(names []string) {
	for _, v := range names {
		go Instance(v).Run()
	}
}
