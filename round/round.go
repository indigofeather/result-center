package round

import (
	"encoding/json"
	"strings"
	"time"

	"bitbucket.org/indigofeather/result-center/consts"
	"bitbucket.org/indigofeather/result-center/db"
	"bitbucket.org/indigofeather/result-center/game"
	"bitbucket.org/indigofeather/result-center/models"
	"github.com/volatiletech/sqlboiler/queries"
	"github.com/volatiletech/sqlboiler/queries/qm"
)

type Status = int

const (
	StatusNew Status = iota
	StatusHasResult
	NotChecked = 0
	Checked    = iota
)

type Round struct {
	ID       uint   `json:"num"`
	Game     string `json:"game_id"`
	RoundID  string `json:"round_id"`
	Result   string `json:"result"`
	Status   int8   `json:"status"` // 0 未開獎 , 1 已開獎 , 2 已停開
	StartAt  int64  `json:"start_at"`
	EndAt    int64  `json:"end_at"`
	DrawAt   int64  `json:"draw_at"`
	CreateAt int64
	UpdateAt int64
	Checked  int8
}

type ListRound struct {
	ID        uint   `json:"round_id"`
	Game      string `json:"game_id"`
	RoundID   string `json:"num"`
	UpdatedAt int64  `json:"updated_at"`
}

type RoundDetail struct {
	RoundID uint           `json:"round_id"`
	Game    string         `json:"game_id"`
	Num     string         `json:"num"`
	Result  map[string]int `json:"result"`
	StartAt int64          `json:"start_at"`
	EndAt   int64          `json:"end_at"`
	DrawAt  int64          `json:"draw_at,omitempty"`
	Status  int8           `json:"status"`  // 0 一般期數狀態（正常開獎不用動） , 1 官方停開 , 2 手動開獎
	Checked int8           `json:"checked"` // 0 未開獎 , 1 已開獎
}

// Forge 建立期數
func Forge(g *game.Game, r []*game.RoundDetail) error {
	for _, v := range r {
		modelRound := models.Round{
			GameID:    g.Name,
			GameRound: v.RoundID,
			StartAt:   v.StartTime,
			EndAt:     v.CloseTime,
		}

		err := modelRound.Upsert(db.DB, nil)
		if err != nil {
			return err
		}
	}
	return nil
}

// GetRoundsByPeriod 取特定時間段的所有期數資料
func GetRoundsByPeriod(from, to time.Time) ([]*ListRound, error) {
	rounds, err := models.Rounds(db.DB,
		qm.Where("start_at > ? AND end_at < ?", from, to),
		qm.Select("id", "game_id", "game_round", "updated_at"),
	).All()
	if err != nil {
		return nil, err
	}

	result := []*ListRound{}
	for _, r := range rounds {
		result = append(result, &ListRound{
			ID:        r.ID,
			Game:      r.GameID,
			RoundID:   r.GameRound,
			UpdatedAt: r.UpdatedAt.Unix(),
		})
	}

	return result, nil
}

// GetRoundByID 以唯一id取特定期數資料（id要先撈時間段取得）
func GetRoundByID(rid int) (*RoundDetail, error) {
	r, err := models.Rounds(db.DB,
		qm.Where("id = ?", rid),
		qm.Select("id", "game_id", "game_round", "result", "start_at", "end_at", "draw_at", "checked", "status"),
	).One()
	if err != nil {
		return nil, err
	}

	res := map[string]int{}
	err = json.Unmarshal([]byte(r.Result), &res)
	draw := r.DrawAt.Unix()
	if draw < 0 {
		draw = 0
	}

	return &RoundDetail{
		RoundID: r.ID,
		Game:    r.GameID,
		Num:     r.GameRound,
		Result:  res,
		StartAt: r.StartAt.Unix(),
		EndAt:   r.EndAt.Unix(),
		DrawAt:  draw,
		Status:  r.Status,
		Checked: r.Checked,
	}, nil
}

// SetResult 錄入賽果
func SetResult(g *game.Game, gameRound string, result *consts.Result) (*models.Round, error) {
	round, err := models.Rounds(db.DB, qm.Where("game_id=? and game_round=?", g.Name, gameRound)).One()
	resultByte, err := json.Marshal(result.Result)
	if err != nil {
		return nil, err
	}
	round.Result = string(resultByte)
	round.Checked = 1
	round.DrawAt = time.Unix(result.Stamp, 0)
	round.UpdatedAt = time.Now()
	err = round.Update(db.DB, "result", "checked", "draw_at", "updated_at")
	if err != nil {
		return nil, err
	}

	return round, nil
}

// Discard 官方關閉某期
func Discard(g *game.Game, gameRound string) error {
	round, err := models.Rounds(db.DB, qm.Where("game_id=? and game_round=?", g.Name, gameRound)).One()
	if err != nil {
		return err
	}
	// 1 為官方停開狀態
	round.Status = 1

	return round.Update(db.DB, "status")
}

// GetUndrawRound 從map中取未有賽果的期數回傳
func GetUndrawRound(g *game.Game, rounds map[string]*consts.Result) ([]string, error) {
	str := strings.Builder{}
	result := []string{}
	type row struct {
		GameRound string `boil:"game_round"`
	}
	rows := []*row{}

	for key, _ := range rounds {
		if str.Len() != 0 {
			str.WriteString(" OR ")
		}
		str.WriteString("`game_round` = '" + key + "'")
	}

	// 組語法
	q := "SELECT `game_round` FROM `rounds` WHERE ((" + str.String() + ") AND `game_id` = '" + g.Name + "') AND `checked` = 0"
	err := queries.Raw(db.DB, q).Bind(&rows)
	if err != nil {
		return nil, err
	}
	if len(rows) < 1 {
		return result, nil
	}

	for _, r := range rows {
		result = append(result, r.GameRound)
	}
	return result, nil
}

type R struct {
	ids uint
}

func CheckResult() {

}
