package main

//# go:generate goose up
//go:generate sqlboiler mysql

import (
	"time"

	"bitbucket.org/indigofeather/result-center/db"
	"bitbucket.org/indigofeather/result-center/round"
	"bitbucket.org/indigofeather/result-center/task"
	"bitbucket.org/indigofeather/result-center/ws"
	"github.com/aviddiviner/gin-limit"
	"github.com/gin-gonic/gin"
	"github.com/volatiletech/sqlboiler/boil"
)

var GitCommit, GitSummary, BuildDate string

func main() {
	boil.DebugMode = true
	startAt := time.Now()

	defer db.DB.Close()
	r := gin.Default()
	r.Use(limit.MaxAllowed(100))
	r.LoadHTMLFiles("index.html")
	r.GET("/", func(c *gin.Context) {
		round.CheckResult()
		c.HTML(200, "index.html", nil)
	})
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"GitCommit":  GitCommit,
			"GitSummary": GitSummary,
			"BuildDate":  BuildDate,
			"servertime": time.Now(),
			"startAt":    startAt,
			"online":     ws.WS.Len(),
		})
	})
	r.GET("/ws", func(c *gin.Context) {
		ws.WS.HandleRequest(c.Writer, c.Request)
	})

	task.Start()
	r.Run(":2000")
}
