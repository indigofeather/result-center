package history

import (
	"fmt"

	"bitbucket.org/indigofeather/result-center/db"
	"bitbucket.org/indigofeather/result-center/models"
	"github.com/volatiletech/sqlboiler/queries/qm"
)

func New() {

}

func CheckRounds(roundIDs []string) {
	rounds, err := models.Histories(db.DB, qm.Where("").WhereIn("round_id in ?", roundIDs))
	fmt.Printf("%+v %s", rounds, err)
}
