package cache

import (
	"bytes"
	"encoding/gob"
	"runtime/debug"

	"github.com/coocood/freecache"
)

var Cache *freecache.Cache

func init() {
	cacheSize := 100 * 1024 * 1024
	Cache = freecache.NewCache(cacheSize)
	debug.SetGCPercent(20)
}

func Serialize(i interface{}) ([]byte, error) {

	if b, ok := i.([]byte); ok {
		return b, nil
	}

	var buf bytes.Buffer

	enc := gob.NewEncoder(&buf)

	if err := enc.Encode(i); err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

func DeSerialize(data []byte, i interface{}) error {

	if b, ok := i.(*[]byte); ok {
		*b = data
		return nil
	}

	return gob.NewDecoder(bytes.NewBuffer(data)).Decode(i)
}
